<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            //
            $table->id();
            $table->foreignId('dataset_id');
            $table->text('lastname');
            $table->text('firstname');
            $table->date('birthdate')->nullable();
            $table->integer('age');
            $table->string('gender');
            $table->text('groups');
            $table->decimal('usual_height');
            $table->decimal('usual_weight');
            $table->string('uuid');
            $table->text('diag');
            $table->string('hash')->nullable();
            $table->text('med_name')->nullable();
            $table->text('med_service')->nullable();
            $table->datetime('last_updated')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
};
