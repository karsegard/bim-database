<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mesures', function (Blueprint $table) {
            //

            $table->id();
            $table->foreignId('subject_id')->nullable()->constrained()->onDelete('set null');
            $table->integer('mesure_id');
            $table->date('date');
            $table->string('examinator');
            $table->decimal('height');
            $table->decimal('weight');
            $table->decimal('bmi');
            $table->boolean('left_side');
            $table->text('machine');
            $table->boolean('smoker');
            $table->decimal('bmi_ref')->nullable();
            $table->string('status');
            $table->decimal('ideal_weight');
            $table->decimal('pct_ideal_weight');
            $table->string('most_accurate_formula');
            $table->integer('current_age');
            $table->text('sport')->nullable();
            $table->string('uuid');
            $table->text('comments');
            $table->string('hash')->nullable();
            $table->string('subject_uuid');
            $table->text('fds')->nullable();
            $table->text('data')->nullable();
            $table->text('bia_data')->nullable();
            $table->datetime('last_updated')->nullable();

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mesures');
    }
};
