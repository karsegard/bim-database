<?php

namespace KDA\Bim\Database\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Bim\Database\Models\Mesure;
use KDA\Bim\Database\Models\Subject;

class MesureFactory extends Factory
{
    protected $model = Mesure::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
          'mesure_id'=>$this->faker->numberBetween(),
          'date'=>$this->faker->date(),
          'examinator'=>$this->faker->firstName(),
          'left_side'=>$this->faker->boolean(),
          'smoker'=>$this->faker->boolean(),
          'machine'=>$this->faker->word(),
          'status'=>$this->faker->randomElement(['active','deleted']),
          'most_accurate_formula'=>$this->faker->randomElement(['kushner','segal']),
          'sport'=>"[]",
          'uuid'=>$this->faker->uuid(),
          'subject_id'=>Subject::factory(),
          'comments'=>"[]",
          'subject_uuid'=>$this->faker->uuid(),
          
        ];
    }
}
