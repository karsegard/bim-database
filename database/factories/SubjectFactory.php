<?php

namespace KDA\Bim\Database\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Bim\Database\Models\Dataset;
use KDA\Bim\Database\Models\Subject;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Subject>
 */
class SubjectFactory extends Factory
{
    protected $model = Subject::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
           'dataset_id'=>Dataset::factory(),
           'lastname'=>$this->faker->lastName(),
           'firstname'=>$this->faker->firstName(),
           'gender'=>$this->faker->randomElement(['F','M']),
           'usual_weight'=>$this->faker->numberBetween(30,120),
           'uuid'=>$this->faker->uuid(),
        ];
    }
}
