<?php

namespace KDA\Bim\Database\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Bim\Database\Models\Dataset;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Dataset>
 */
class DatasetFactory extends Factory
{
    protected $model = Dataset::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
           'name'=>$this->faker->words(2,true),
        ];
    }
}
