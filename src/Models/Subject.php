<?php

namespace KDA\Bim\Database\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ESolution\DBEncryption\Traits\EncryptedAttribute;
use KDA\Bim\Database\Database\Factories\SubjectFactory;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;

class Subject extends Model
{
    use HasFactory;
   // use EncryptedAttribute;
    use HasDefaultAttributes;

    public $encryptable=[
        'lastname',
        'firstname',
        'diag',
        'med_name',
        'med_service'
    ];
    
    public $fillable= [
        'dataset_id',
        'id',
        'lastname',
        'firstname',
        'birthdate',
        'age',
        'gender',
        'groups',
        'usual_height',
        'usual_weight',
        'uuid',
        'diag',
        'hash',
        'last_updated',
        'med_name',
        'med_service',
        'created_at',
        'updated_at',
    ];

    public $casts = [
        'diag'=>'array',
        'groups'=>'array',
        'comments'=>'array',
        'sport'=>'array',
    ];

    public $encrypteds=[
        'lastname',
        'firstname',
        'birthdate',
        'diag'
    ];
    protected static function newFactory()
    {
        return  SubjectFactory::new();
    }

 
    public function applyDefaultAttributes()
    {
        $this->defaultAttribute('birthdate',null);
        $this->defaultAttribute('age',0);
        $this->defaultAttribute('usual_height',0);
        $this->defaultAttribute('groups',"[]");
        $this->defaultAttribute('diag',"[]");
        $this->defaultAttribute('usual_weight',"0");
        if(empty($this->attributes['age'])){
            $this->attributes['age']=0;
        }
    }
   
 
}
