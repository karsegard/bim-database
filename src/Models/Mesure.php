<?php

namespace KDA\Bim\Database\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Bim\Database\Database\Factories\MesureFactory;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
class Mesure extends Model
{
    use HasFactory;
    use HasDefaultAttributes;
    public $fillable= [
        'id',
        'subject_id',
        'mesure_id',
        'date',
        'examinator',
        'height',
        'weight',
        'bmi',
        'left_side',
        'machine',
        'smoker',
        'bmi_ref',
        'status',
        'ideal_weight',
        'pct_ideal_weight',
        'most_accurate_formula',
        'current_age',
        'sport',
        'uuid',
        'comments',
        'hash',
        'subject_uuid',
        'last_updated',
        'fds',
        'data',
        'bia_data',
        'created_at',
        'updated_at',
        
    ];

    public $casts = [
        'fds'=>'array',
        'bia_data'=>'array',
        'data'=>'array',
        
    ];
    protected static function newFactory()
    {
        return  MesureFactory::new();
    }

 
    public function applyDefaultAttributes()
    {
        if(empty($this->attributes['ideal_weight']) || !is_numeric($this->attributes['ideal_weight'])){
            $this->attributes['ideal_weight']=0;
        }
        if(empty($this->attributes['bmi_ref'])){
            $this->attributes['bmi_ref']=0;
        }
        if(empty($this->attributes['height'])){
            $this->attributes['height']=0;
        }
        if(empty($this->attributes['weight'])){
            $this->attributes['weight']=0;
        }
        if(empty($this->attributes['bmi']) || !is_numeric($this->attributes['bmi'])){
            $this->attributes['bmi']=0;
        }
        if(empty($this->attributes['pct_ideal_weight'])){
            $this->attributes['pct_ideal_weight']=0;
        }
        if(empty($this->attributes['current_age'])){
            $this->attributes['current_age']=0;
        }
        if(empty($this->attributes['last_updated'])){
            $this->attributes['last_updated']=NULL;
        }
        if(empty($this->attributes['mesure_id'])){
            $this->attributes['mesure_id']=1;
        }
        if(empty($this->attributes['smoker'])){
            $this->attributes['smoker']=0;
        }
      
    }

    public function fixDecimalValue($value){
        if(blank($value)){
            $value = 0;
        }
        return str_replace(',','.',$value);
    }

    public function setHeightAttribute($value){
        $this->attributes['height']=$this->fixDecimalValue($value);
    }

}
