<?php

namespace KDA\Bim\Database\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ESolution\DBEncryption\Traits\EncryptedAttribute;
use KDA\Bim\Database\Database\Factories\DatasetFactory;
use KDA\Bim\Database\Database\Factories\SubjectFactory;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;

class Dataset extends Model
{
    use HasFactory;

    public $fillable= [
        'name','computer','notes'
    ];

   
    protected static function newFactory()
    {
        return  DatasetFactory::new();
    }

 
   

}
