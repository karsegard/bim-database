<?php

namespace KDA\Bim\Database\Facades;

use Illuminate\Support\Facades\Facade;

class BimDatabase extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
