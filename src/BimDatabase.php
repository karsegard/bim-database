<?php

namespace KDA\Bim\Database;

use KDA\Bim\Database\Models\Dataset;
use KDA\Bim\Database\Models\Mesure;
use KDA\Bim\Database\Models\Subject;
use KDA\CSVSeeder\Facades\CSV;

//use Illuminate\Support\Facades\Blade;
class BimDatabase
{
    public function importDataset($file,$offset=0)
    {
        $dataset_id = Dataset::firstOrCreate(['name' => $file])->id;
        $count = -1;
        CSV::makeParser($file)->header(true)
            ->handleRowUsing(function ($mappedRow, $rawRow, $mapping) use ($dataset_id,$offset,&$count) {
                $count++;

                if($count<$offset){
                    return;
                }
               
                dump($mappedRow);
                $col = collect($mappedRow);
                $json_keys = ['diag', 'groups'];
                dump($mappedRow['mesures.status']);
                if ($mappedRow['mesures.status'] == "deleted") {
                    return;
                }
                $filled = false;
                $subject = $col->filter(function ($item, $key) {
                    return stripos($key, 'subjects.') !== false && $key != 'subjects.id';
                })->mapWithKeys(function ($item, $key) use ($json_keys,&$filled) {
                    $key = str_replace('subjects.', '', $key);
                    if (in_array($key, $json_keys)) {
                        $item = json_decode($item, true);
                    }
                    if ($item == 'null') {
                        $item = null;
                    }
                    if(!blank($item) && $key !="hash"){
                        $filled = true;
                    }
                    return [$key => $item];
                });

                if (empty($subject['last_updated'])) {
                    $subject['last_updated'] = NULL;
                }
                dump($subject->toArray());

                $s = Subject::updateOrCreate(
                    ['uuid' => $subject['uuid'], 'dataset_id' => $dataset_id],
                    $subject->toArray()
                );

                $json_keys = ['fds', 'data', 'bia_data'];
                $filled = false;
                $mesure = $col->filter(function ($item, $key) {
                    return stripos($key, 'mesures.') !== false  && $key != 'mesures.id' && $key != 'mesures.subject_id';
                })->mapWithKeys(function ($item, $key) use ($json_keys,&$filled) {
                    $key = str_replace('mesures.', '', $key);
                    if (in_array($key, $json_keys)) {
                        $item = json_decode($item, true);
                    }
                    if ($item == 'null') {
                        $item = null;
                    }
                    if(!blank($item)  && $key !="hash"){
                        $filled = true;
                    }
                    return [$key => $item];
                });

                dump($mesure->toArray());
                if ($mesure['status'] !== 'deleted' &&$filled) {

                    Mesure::updateOrCreate(
                        ['uuid' => $mesure['uuid'], 'subject_id' => $s->id],
                        $mesure->toArray()
                    );
                }
            })->parse();
    }
}
