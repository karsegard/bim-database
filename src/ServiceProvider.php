<?php
namespace KDA\Bim\Database;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
use KDA\Bim\Database\Facades\BimDatabase as Facade;
use KDA\Bim\Database\BimDatabase as Library;
use KDA\Bim\Database\Commands\ImportFile;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasLoadableMigration;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use HasLoadableMigration;
    use HasCommands;
    protected $packageName ='bim-database';
    protected $_commands = [
        ImportFile::class
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
