<?php

namespace KDA\Bim\Database\Commands;

use Illuminate\Console\Command;
use KDA\Laravel\BackupServer\Facades\BackupServer;
use KDA\Bim\Database\Facades\BimDatabase;

class ImportFile extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bim:import:file {name} {--offset=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->argument('name');
        $offset = $this->option('offset') ?? 0;
        BimDatabase::importDataset($name , intval($offset));

        
    }
}
